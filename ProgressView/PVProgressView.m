//
//  PVProgressView.m
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import "PVProgressView.h"
#import <QuartzCore/QuartzCore.h>

@interface PVProgressViewLayer : CALayer

@property(nonatomic, strong) UIColor *trackTintColor;
@property(nonatomic, strong) UIColor *progressTintColor;
@property(nonatomic) NSInteger roundedCorners;
@property(nonatomic) CGFloat thicknessRatio;
@property(nonatomic) CGFloat progress;

@end

@implementation PVProgressViewLayer

@dynamic trackTintColor;
@dynamic progressTintColor;
@dynamic roundedCorners;
@dynamic thicknessRatio;
@dynamic progress;

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    return [key isEqualToString:@"progress"] ? YES : [super needsDisplayForKey:key];
}

- (void)drawInContext:(CGContextRef)ctx
{
    CGRect rect = self.bounds;
    CGPoint centerPoint = CGPointMake(rect.size.height / 2.0f, rect.size.width / 2.0f);
    CGFloat radius = MIN(rect.size.height, rect.size.width) / 2.0f;
    
    CGFloat progress = MIN(self.progress, 1.0f - FLT_EPSILON);
    CGFloat radians = (progress * 2.0f * M_PI) - M_PI_2;
    
    CGContextSetFillColorWithColor(ctx, self.trackTintColor.CGColor);
    CGMutablePathRef trackPath = CGPathCreateMutable();
    CGPathMoveToPoint(trackPath, NULL, centerPoint.x, centerPoint.y);
    CGPathAddArc(trackPath, NULL, centerPoint.x, centerPoint.y, radius, 3.0f * M_PI_2, -M_PI_2, NO);
    CGPathCloseSubpath(trackPath);
    CGContextAddPath(ctx, trackPath);
    CGContextFillPath(ctx);
    CGPathRelease(trackPath);
    
    if (progress > 0.0f)
    {
        CGContextSetFillColorWithColor(ctx, self.progressTintColor.CGColor);
        CGMutablePathRef progressPath = CGPathCreateMutable();
        CGPathMoveToPoint(progressPath, NULL, centerPoint.x, centerPoint.y);
        CGPathAddArc(progressPath, NULL, centerPoint.x, centerPoint.y, radius, 3.0f * M_PI_2, radians, NO);
        CGPathCloseSubpath(progressPath);
        CGContextAddPath(ctx, progressPath);
        CGContextFillPath(ctx);
        CGPathRelease(progressPath);
    }
    
    if (progress > 0.0f && self.roundedCorners)
    {
        CGFloat pathWidth = radius * self.thicknessRatio;
        CGFloat xOffset = radius * (1.0f + ((1.0f - (self.thicknessRatio / 2.0f)) * cosf(radians)));
        CGFloat yOffset = radius * (1.0f + ((1.0f - (self.thicknessRatio / 2.0f)) * sinf(radians)));
        CGPoint endPoint = CGPointMake(xOffset, yOffset);
        
        CGContextAddEllipseInRect(ctx, CGRectMake(centerPoint.x - pathWidth / 2.0f, 0.0f, pathWidth, pathWidth));
        CGContextFillPath(ctx);
        
        CGContextAddEllipseInRect(ctx, CGRectMake(endPoint.x - pathWidth / 2.0f, endPoint.y - pathWidth / 2.0f, pathWidth, pathWidth));
        CGContextFillPath(ctx);
    }
    
    CGContextSetBlendMode(ctx, kCGBlendModeClear);
    CGFloat innerRadius = radius * (1.0f - self.thicknessRatio);
    CGPoint newCenterPoint = CGPointMake(centerPoint.x - innerRadius, centerPoint.y - innerRadius);
    CGContextAddEllipseInRect(ctx, CGRectMake(newCenterPoint.x, newCenterPoint.y, innerRadius * 2.0f, innerRadius * 2.0f));
    CGContextFillPath(ctx);

}

@end



@implementation PVProgressView



+ (void)initialize
{
    if (self != [PVProgressView class]){
        return;
    }
    id appearance = [self appearance];
    [appearance setTrackTintColor:[[UIColor whiteColor] colorWithAlphaComponent:0.3f]];
    [appearance setProgressTintColor:[UIColor whiteColor]];
    [appearance setBackgroundColor:[UIColor clearColor]];
    [appearance setThicknessRatio: 0.3f];
    [appearance setRoundedCorners:YES];
    
    [appearance setIndeterminateDuration:2.0f];
    [appearance setIndeterminate:NO];
    
}

+ (Class)layerClass
{
    return [PVProgressViewLayer class];
}
- (PVProgressViewLayer *)circularProgressLayer
{
    return (PVProgressViewLayer *)self.layer;
}
- (id)init
{
    
    return [super initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
}
- (void)didMoveToWindow
{
    CGFloat windowContentsScale = self.window.screen.scale;
    self.circularProgressLayer.contentsScale = windowContentsScale;
}
#pragma mark - 
#pragma mark - Progress Methods

- (CGFloat)progress
{
    
    return self.circularProgressLayer.progress;
}

- (void)setProgress:(CGFloat)progress
{
    [self setProgress:progress animated:NO];
}


-(void)setProgress:(CGFloat)progress animated:(BOOL)animated
{
    CGFloat pinProgress =MIN(MAX(progress, 0.0f), 1.0f);
    self.progressLabel.text=[NSString stringWithFormat:@"%@%%",[NSNumber numberWithInt:pinProgress*100]];;
    if (animated) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"progress"];
        animation.duration=fabs(self.progress - pinProgress);
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.fromValue = [NSNumber numberWithFloat:self.progress];
        animation.toValue = [NSNumber numberWithFloat:pinProgress];
        [self.circularProgressLayer addAnimation:animation forKey:@"progress"];
    }
    else {
        [self.circularProgressLayer setNeedsDisplay];
    }
    self.circularProgressLayer.progress=pinProgress;


    
}

#pragma mark - 
#pragma mark - Appearance Methods


- (UIColor *)trackTintColor
{
    return self.trackTintColor;
}

- (void)setTrackTintColor:(UIColor *)trackTintColor
{
    self.circularProgressLayer.trackTintColor=trackTintColor;
    [self.circularProgressLayer setNeedsDisplay];
}

- (UIColor *)progressTintColor
{
    return self.progressTintColor;
}

- (void)setProgressTintColor:(UIColor *)progressTintColor
{
    self.circularProgressLayer.progressTintColor=progressTintColor;
    [self.circularProgressLayer setNeedsDisplay];
}

- (NSInteger)roundedCorners
{
    return self.roundedCorners;
}

- (void)setRoundedCorners:(NSInteger)roundedCorners
{
    self.circularProgressLayer.roundedCorners=roundedCorners;
    [self.circularProgressLayer setNeedsDisplay];
}

- (CGFloat)thicknessRatio
{
    return self.circularProgressLayer.thicknessRatio;
}

- (void)setThicknessRatio:(CGFloat)thicknessRatio
{   
    self.circularProgressLayer.thicknessRatio = MIN(MAX(thicknessRatio, 0.f), 1.f);
    [self.circularProgressLayer setNeedsDisplay];
}

- (NSInteger)indeterminate
{
    CAAnimation *spin =[self.layer animationForKey:@"indeterminateAnimation"];
    return (spin == nil ? 0 : 1);
}

- (void) setIndeterminate:(NSInteger)indeterminate
{
    if (indeterminate && !self.indeterminate) {
        CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        spin.byValue=[NSNumber numberWithFloat:2.0f*M_PI];
        spin.duration=self.indeterminateDuration;
        spin.repeatCount = HUGE_VALF;
        [self.layer addAnimation:spin forKey:@"indeterminateAnimation"];
    }
    else {
        [self.layer removeAnimationForKey:@"indeterminateAnimation"];
    }
}

#pragma mark - 
#pragma mark - Label creation
- (void)createLabelWithColor: (UIColor *)color
{
    UILabel *progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width/1.5f, self.bounds.size.height/1.5f)];
    progressLabel.center=CGPointMake(self.bounds.size.width /2, self.bounds.size.height /2);
    progressLabel.font= [UIFont fontWithName:@"Helvetica-BoldOblique" size:12.0];
    progressLabel.textColor=color;
    progressLabel.textAlignment=NSTextAlignmentCenter;
    progressLabel.backgroundColor=[UIColor clearColor];
    progressLabel.text=[NSString stringWithFormat:@"%@%%",[NSNumber numberWithFloat:self.progress*100]];
    self.progressLabel=progressLabel;
    [self addSubview:self.progressLabel];
 
        
}

@end
