//
//  PVIndicatorAlertView.h
//  ProgressView
//
//  Created by Kate on 15.07.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVIndicatorAlertView : UIView
- (id)initWithTitle: (NSString *)title;
- (id)initWithTitle:(NSString *)title andProgressLabel: (NSInteger)progressLabel andIndeterminate: (NSInteger)indeterminate;
- (void)updateUIWithProgress:(float)progress;
- (void)show;
- (void)hide;
@end
