//
//  PVViewController.h
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PVProgressView.h"
#import "PVIndicatorAlertView.h"


@interface PVViewController : UIViewController
@property (nonatomic,strong)  PVProgressView *progressView;

@property (weak, nonatomic) IBOutlet PVProgressView *largeProgress;
@property (nonatomic, strong) PVIndicatorAlertView *pvAlertView;
@end
