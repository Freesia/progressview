//
//  PVProgressView.h
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVProgressView : UIView

@property (nonatomic,strong) UIColor *trackTintColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *progressTintColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat thicknessRatio UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat progress;
@property (nonatomic) NSInteger roundedCorners UI_APPEARANCE_SELECTOR;
@property (nonatomic) NSInteger indeterminate UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indeterminateDuration UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UILabel * progressLabel;
@property (nonatomic) NSInteger progressLabelCreation;

-(void)setProgress:(CGFloat )progress animated:(BOOL)animated;
- (void)createLabelWithColor: (UIColor *)color;
@end 
