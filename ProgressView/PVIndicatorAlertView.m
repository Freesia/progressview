//
//  PVIndicatorAlertView.m
//  ProgressView
//
//  Created by Kate on 15.07.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import "PVIndicatorAlertView.h"
#import <QuartzCore/QuartzCore.h>
#import "PVProgressView.h"

@interface PVIndicatorAlertView ()

@property (nonatomic,strong) UILabel * titleLabel;
@property UIInterfaceOrientation interfaceOrientation;
@property (nonatomic, strong) PVProgressView *progressIndicator;

@end

@implementation PVIndicatorAlertView


- (id)initWithTitle:(NSString *)title
{
    self = [self initWithTitle:title andProgressLabel:NO andIndeterminate:NO];
    return self;
}
- (id)initWithTitle:(NSString *)title andProgressLabel: (NSInteger)progressLabel andIndeterminate:(NSInteger)indeterminate
{
    self = [super initWithFrame:CGRectMake(0, 0, 280, 220)];
    
    self.layer.cornerRadius = 6;
    self.layer.borderColor = [[UIColor blackColor] CGColor];
    self.layer.borderWidth = 2;
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.7f;
    self.layer.shadowRadius = 5.0f;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:.65];
    self.alpha = 0;
    
    self.progressIndicator = [[PVProgressView alloc] initWithFrame:CGRectMake(105, 105, 70, 70)];
    self.progressIndicator.progressTintColor=[UIColor whiteColor];
    if (progressLabel) {
        [self.progressIndicator createLabelWithColor:[UIColor whiteColor]];
    }
    if (indeterminate) {
        self.progressIndicator.progress = 0.3;
        self.progressIndicator.indeterminateDuration=3.0f;
        self.progressIndicator.indeterminate = YES;
    }
    [self addSubview:self.progressIndicator];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, 240, 30)];
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.titleLabel setText:title];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self addSubview:self.titleLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIApplicationDidChangeStatusBarOrientationNotification"
                                               object:nil];
    
    return self;
}

#pragma mark - Show and hide

- (void)show
{
    CGRect fullscreenRect = [self getScreenBoundsForCurrentOrientation];
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    if (window.rootViewController.presentedViewController) {
        [window.rootViewController.presentedViewController.view addSubview:self];
    } else {
        [window.rootViewController.view addSubview:self];
    }
    self.center = CGPointMake(fullscreenRect.size.width / 2, fullscreenRect.size.height / 2);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
        
    } completion:^(BOOL finished){
        
    }];

}

- (void)hide
{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}

#pragma mark - Orientation helper methods

- (CGRect)getScreenBoundsForCurrentOrientation
{
    return [self getScreenBoundsForOrientation:self.interfaceOrientation];
}

- (CGRect)getScreenBoundsForOrientation:(UIInterfaceOrientation)orientation
{
    UIScreen *screen = [UIScreen mainScreen];
    CGRect screenRect = screen.bounds;

    if (UIDeviceOrientationIsLandscape(orientation)) {
        CGRect temp;
        temp.size.width = screenRect.size.height;
        temp.size.height = screenRect.size.width;
        screenRect = temp;
    }
    
    return screenRect;
}

#pragma mark - Handle changes to viewport

- (void)didRotate:(NSNotification *)notification
{
    self.interfaceOrientation = [UIDevice currentDevice].orientation;
    
    [self didChangeScreenBounds];
}
- (void)didChangeScreenBounds
{
    CGRect screenRect = [self getScreenBoundsForCurrentOrientation];
    
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [UIView animateWithDuration:0.5 animations:^{
        self.center = CGPointMake(screenWidth / 2, screenHeight / 2);
    }];
}

- (void)updateUIWithProgress:(float)progress
{
    if (floor(progress) == 100)
    {
        [self performSelector:@selector(hide) withObject:nil afterDelay:1];
    }
    
    self.progressIndicator.progress=progress;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.progressIndicator = nil;
    self.titleLabel = nil;
    [self removeFromSuperview];
}


@end
