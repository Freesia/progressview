//
//  PVAppDelegate.h
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
