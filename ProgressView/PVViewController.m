//
//  PVViewController.m
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import "PVViewController.h"

@interface PVViewController ()
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation PVViewController
@synthesize progressView = _progressView;
@synthesize largeProgress=_largeProgress;
@synthesize timer=_timer;

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self startAnimation];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.progressView = [[PVProgressView alloc] initWithFrame:CGRectMake(140.0f, 30.0f, 60.0f, 60.0f)];
    self.progressView.progressTintColor = [UIColor blueColor];
    self.progressView.indeterminate=NO;
    [self.progressView createLabelWithColor:[UIColor blueColor]];
    [self.view addSubview:self.progressView];
    self.largeProgress.progressTintColor=[UIColor blackColor];
    self.largeProgress.progress=0.3;
    self.largeProgress.indeterminate=YES;
   
        
}
- (void)startAnimation
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.06  target:self selector:@selector(progressChange) userInfo:nil repeats:YES];
   }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)progressChange
{
        
        CGFloat progress =  self.progressView.progress + 0.01f;
        [self.progressView setProgress:progress animated:YES];
    
        if (self.progressView.progress >= 1.0f && [self.timer isValid])
        {
            [self.progressView setProgress:0.f animated:YES];
        }
        
        
    }

- (IBAction)showIndicatorAlert:(id)sender {
    self.pvAlertView = [[PVIndicatorAlertView alloc] initWithTitle:@"Test" andProgressLabel:NO andIndeterminate:YES];
    [self.pvAlertView show];
}
- (IBAction)hideIndicatorAlert:(id)sender {
    [self.pvAlertView hide];
}

@end
