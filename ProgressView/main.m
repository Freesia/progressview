//
//  main.m
//  ProgressView
//
//  Created by Kate on 24.06.13.
//  Copyright (c) 2013 Kate. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PVAppDelegate class]));
    }
}
